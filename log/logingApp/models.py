from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=50)
    street = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    class Meta:
        ordering = ('id',)
    def __str__(self):
        return self.name

class Factory(models.Model):
    name = models.CharField(max_length=50)
    desctription = models.TextField()
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    class Meta:
        ordering = ('company','id')
    def __str__(self):
        return self.name

class Machine(models.Model):
    name = models.CharField(max_length=50)
    desctription = models.TextField()
    type = models.CharField(max_length=50)
    factory = models.ForeignKey(Factory, on_delete=models.CASCADE)
    class Meta:
        ordering = ('factory','id')
    def __str__(self):
        return self.name

class User(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    department = models.CharField(max_length=20)
    phoneNumber = models.CharField(max_length=20)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    def __str__(self):
        return self.user.username
