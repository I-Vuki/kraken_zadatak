from rest_framework import serializers
from logingApp.models import Company, Factory

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'street', 'city')

class FactorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Factory
        fields = ('id', 'name', 'desctription', 'company')
