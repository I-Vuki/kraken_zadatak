from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Company)
admin.site.register(models.Factory)
admin.site.register(models.Machine)
admin.site.register(models.User)
