from django.shortcuts import get_object_or_404
from logingApp.models import Company, User, Factory, Machine
from logingApp.serializers import CompanySerializer, FactorySerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.
class CompanysListView(LoginRequiredMixin, APIView):
	login_url = ''
	redirect_field_name = ''
	renderer_classes = [TemplateHTMLRenderer]
	template_name = 'logingApp/company_list.html'

	def get(self, request):
		company = User.objects.filter(user = self.request.user).first().company
		factorys =  Factory.objects.filter(company=company)
		cSerializer = CompanySerializer(company)
		fSerializer = FactorySerializer(factorys, many=True)

		context = {}
		context ['companySerializer'] = cSerializer
		context ['factorysSerializer'] = fSerializer

		return Response(context)
