from django import template
from logingApp.models import Machine
register = template.Library()

@register.filter
def machines(factory):
    ret = Machine.objects.filter(factory=factory)
    return ret
